//
//  MoviesListCell.swift
//  Movies
//
//  Created by Siva Kumar Reddy Thimmareddy on 04/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import UIKit

class MoviesListCell: UITableViewCell {
    var imageCache = [String: UIImage]()

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieCategory: UILabel!
    @IBOutlet weak var releaseDate: UILabel!

    private let sessionProvider = URLSessionProvider()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func initializeCell(movie: Entry) {
        if let name = movie.imName?.label {
            movieName?.text = name
        }
        if  let category = movie.category?.attributes?.label {
            movieCategory?.text = category
        }
        if  let date = movie.imReleaseDate?.attributes?.label {
            releaseDate?.text = date
        }

        if  let imimage = movie.imImage?[2], let urlString = imimage.label {
            if let img = imageCache[urlString] {
                self.thumbnail?.image = img
            } else {
                sessionProvider.setImageFromUrl(imageUrlStr: urlString) { (data) in
                    DispatchQueue.main.async {
                        self.thumbnail?.image = UIImage(data: data)
                        self.imageCache[urlString] = UIImage(data: data)

                    }
                }

            }
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
