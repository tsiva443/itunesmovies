//
//  MoviesViewController.swift
//  Movies
//
//  Created by Siva Kumar Reddy Thimmareddy on 02/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import UIKit
import SplunkMint

class MoviesViewController: UIViewController {
    private let sessionProvider = URLSessionProvider()
    var moviesListArray: [Entry]?
    @IBOutlet weak var tableView: UITableView!
    var viewModel = MoviesViewModel()

    var filteredTableData = [Entry]()

    var shouldShowSearchResults = false

    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Movies"
        DispatchQueue.global(qos: .background).async {
            self.getMoviesList()
        }
        self.logTransaction()
        searchBar.delegate = self
    }

    private func getMoviesList() {

        viewModel.getMoviewList(type: MoviesModel.self, service: PostService.getMoviesList) { response in
            switch response {
            case let .successWith(posts):
                print(posts)
                if let list = posts.feed?.entry {
                    self.moviesListArray = list
                    DispatchQueue.main.async {
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                        self.tableView.reloadData()
                    }
                }
            case let .failureWith(error):
                print(error)
            }
        }

    }
    func logTransaction() {
        //Creating a local instance of MintLimitedExtraData to pass into our api calls
        let myExtraData = MintLimitedExtraData()
        myExtraData.setValue("\(PostService.getMoviesList.baseURL.absoluteString)", forKey: "url")

        let transactionId = Mint.sharedInstance().transactionStart("MoviesList", extraData: myExtraData)
        //Stopping  transactions, will have status=SUCCESS
        Mint.sharedInstance().transactionStop(transactionId, extraData: myExtraData)

        //Cancelling 3 transactions, will have status=CANCEL
        Mint.sharedInstance().transactionCancel(transactionId, reason: "bad_transaction", extraData: myExtraData)

    }

}

extension MoviesViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shouldShowSearchResults ? filteredTableData.count : moviesListArray?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesListCell", for: indexPath) as? MoviesListCell else { return UITableViewCell() }
        let movie = shouldShowSearchResults ? filteredTableData[indexPath.row] : moviesListArray?[indexPath.row]
        cell.initializeCell(movie: movie!)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if searchBar != nil {
            searchBar.resignFirstResponder()
        }

        let movieEntry = shouldShowSearchResults ? filteredTableData[indexPath.row] : moviesListArray?[indexPath.row]

        let movieDetailVC: MovieDetailViewController  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MovieDetailViewController") as MovieDetailViewController
        movieDetailVC.movie = movieEntry
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }

    func filterSearchController(_ searchText: String) {
        filteredTableData.removeAll(keepingCapacity: false)
        let filteredArray = moviesListArray?.filter { (entry) -> Bool in
            if let name = entry.imName?.label, let category = entry.category?.attributes?.label {
                if name.contains(searchText) || category.contains(searchText) {
                    return true
                }
            }

            return false
        }
        self.filteredTableData = filteredArray ?? []

        self.tableView.reloadData()
    }
}

extension MoviesViewController: UISearchBarDelegate {

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

        if shouldShowSearchResults &&  searchBar.text!.count > 0 {
            filterSearchController(searchBar.text ?? "")
        } else {
            shouldShowSearchResults = false
            self.tableView.reloadData()
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        shouldShowSearchResults = false
        searchBar.resignFirstResponder()
        self.tableView.reloadData()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            shouldShowSearchResults = false
            self.tableView.reloadData()
            return
        }
        shouldShowSearchResults = true
        filterSearchController(searchBar.text ?? "")
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        shouldShowSearchResults = true
        searchBar.resignFirstResponder()
    }

}
