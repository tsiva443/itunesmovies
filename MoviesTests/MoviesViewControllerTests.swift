//
//  MoviesViewModelTests.swift
//  MoviesTests
//
//  Created by Siva Kumar Reddy Thimmareddy on 06/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import XCTest
@testable import Movies

class MoviesViewControllerTests: XCTestCase {
    var viewModel: MoviesViewModel?
    var movieEntries: [Entry]? = []
    var moviesVC = MoviesViewController()

//    var moviesVC: MoviesViewController  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MoviesViewController") as MoviesViewController

    override func setUp() {
        super.setUp()
        viewModel = MoviesViewModel()
         moviesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MoviesViewController") as MoviesViewController

    }

    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testSUT_CanBeInstantiated() {
        XCTAssertNotNil(moviesVC)
    }

    func test_fetch_local_movies() {

        // Given A apiservice
        let viewModel1 = self.viewModel!

        // When fetch popular photo
        let expect = XCTestExpectation(description: "callback")
        viewModel1.fetchLocalMoviesList(type: MoviesModel.self) { (response) in
            expect.fulfill()
            switch response {
            case let .success(posts):
                print(posts)
                if let entries = posts.feed?.entry {
                    self.movieEntries = entries
                    DispatchQueue.main.async {
                        self.loadValueCell()
                    }
                    XCTAssertEqual( entries.count, 1)
                    for entry in entries {
                        XCTAssertNotNil(entry.id)
                    }
                }
            case let .failure(error):
                print(error)

            }
        }

        wait(for: [expect], timeout: 3.1)
    }

    func testEmptyValueInDataSource() {

        // giving empty data value
        moviesVC.moviesListArray = []

        let tableView = UITableView()
        tableView.dataSource = moviesVC

        // expected one section
        XCTAssertEqual(tableView.numberOfSections, 1, "Expected one section in table view")

        // expected zero cells
        XCTAssertEqual(tableView.numberOfRows(inSection: 0), 0, "Expected no cell in table view")

    }

    func loadValueCell() {
        moviesVC.moviesListArray = movieEntries

        let tableView = UITableView()
        tableView.dataSource = moviesVC
        tableView.register(MoviesListCell.self, forCellReuseIdentifier: "MoviesListCell")

        // giving data value

        let indexPath = IndexPath(row: 0, section: 0)

        // expected CurrencyCell class
        guard let _ = moviesVC.tableView(tableView, cellForRowAt: indexPath) as? MoviesListCell else {
            XCTAssert(false, "Expected MoviesListCell class")
            return
        }
    }

}
