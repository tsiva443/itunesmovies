//
//  MoviesViewController.swift
//  Movies
//
//  Created by Siva Kumar Reddy Thimmareddy on 02/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController {
    private let sessionProvider = URLSessionProvider()
    var moviesListArray: [Entry]?
    @IBOutlet weak var tableView: UITableView!
    var viewModel = MoviesViewModel()

    var filteredTableData = [Entry]()
    fileprivate let searchController = UISearchController(searchResultsController: nil)
    var shouldShowSearchResults = false

    @IBOutlet weak var searchBar: UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Movies"
        getMoviesList()
        searchBar.delegate = self

    }

    private func getMoviesList() {

        viewModel.getMoviewList(type: MoviesModel.self, service: PostService.getMoviesList) { response in
            switch response {
            case let .successWith(posts):
                print(posts)
                if let list = posts.feed?.entry {
                    self.moviesListArray = list
                    DispatchQueue.main.async {
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                        self.tableView.reloadData()
                    }
                }
            case let .failureWith(error):
                print(error)
            }
        }

    }
}

extension MoviesViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shouldShowSearchResults ? filteredTableData.count : moviesListArray?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesListCell", for: indexPath) as MoviesListCell {
            let movie = shouldShowSearchResults ? filteredTableData[indexPath.row] : moviesListArray?[indexPath.row]
                 cell.initializeCell(movie: movie!)
            return cell
        }

        return UITableViewCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        searchBar.resignFirstResponder()

        let movieEntry = shouldShowSearchResults ? filteredTableData[indexPath.row] : moviesListArray?[indexPath.row]
        let movieDetailVC: MovieDetailViewController  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MovieDetailViewController") as MovieDetailViewController
        movieDetailVC.movie = movieEntry
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }

    func filterSearchController(_ searchText: String) {
        filteredTableData.removeAll(keepingCapacity: false)
        let array  = moviesListArray?.filter { ($0.imName?.label?.contains(searchText) ?? false ) }

        filteredTableData = array!

        self.tableView.reloadData()
    }
}

extension MoviesViewController: UISearchBarDelegate {

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       if let searchText = searchBar.text {
            if shouldShowSearchResults && searchText.count > 0 {
                       filterSearchController(searchText ?? "")
                   } else {
                       shouldShowSearchResults = false
                       self.tableView.reloadData()
                   }
        }

    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        shouldShowSearchResults = false
        searchBar.resignFirstResponder()
        self.tableView.reloadData()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            shouldShowSearchResults = false
            self.tableView.reloadData()
            return
        }
            shouldShowSearchResults = true
            filterSearchController(searchBar.text ?? "")
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        shouldShowSearchResults = true
        searchBar.resignFirstResponder()
    }

}
