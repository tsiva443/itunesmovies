//
//  MovieDetailViewController.swift
//  Movies
//
//  Created by Siva Kumar Reddy Thimmareddy on 04/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    var movie: Entry?
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let name = movie?.imName?.label {
            self.navigationItem.title = name
        }
        // Do any additional setup after loading the view.
    }

}

extension MovieDetailViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieDetailCell", for: indexPath) as? MovieDetailCell else { return UITableViewCell() }
        if let movieEntry = movie {
            cell.initializeCell(movie: movieEntry)
        }

        return cell

    }

}
