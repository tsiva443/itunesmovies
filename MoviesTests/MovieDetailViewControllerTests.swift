//
//  MovieDetailViewControllerTests.swift
//  MoviesTests
//
//  Created by Siva Kumar Reddy Thimmareddy on 06/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import XCTest
@testable import Movies

class MovieDetailViewControllerTests: XCTestCase {
    let movieDetailVC: MovieDetailViewController  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MovieDetailViewController") as MovieDetailViewController
    var viewModel: MoviesViewModel?
    var movieEntries: [Entry]? = []

   override func setUp() {
             super.setUp()
             viewModel = MoviesViewModel()
    movieDetailVC.viewDidLoad()
         }

         override func tearDown() {
             viewModel = nil
             super.tearDown()
         }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func test_fetch_local_movies() {

        // Given A apiservice
        let viewModel1 = self.viewModel!

        // When fetch popular photo
        let expect = XCTestExpectation(description: "callback")
        viewModel1.fetchLocalMoviesList(type: MoviesModel.self) { (response) in
            expect.fulfill()
            switch response {
            case let .success(posts):
                print(posts)
                if let entries = posts.feed?.entry {
                    self.movieEntries = entries
                    DispatchQueue.main.async {
                        self.loadValueCell()
                    }
                    XCTAssertEqual( entries.count, 1)
                        for entry in entries {
                            XCTAssertNotNil(entry.id)
                   }
                }
            case let .failure(error):
                print(error)

            }
        }

        wait(for: [expect], timeout: 3.1)
    }
        func testEmptyValueInDataSource() {
    //        let tableView = moviesVC.tableView

            // giving empty data value
            movieDetailVC.movie = nil

            let tableView = UITableView()
            tableView.dataSource = movieDetailVC

            // expected one section
            XCTAssertEqual(tableView.numberOfSections, 1, "Expected one section in table view")

            // expected zero cells
            XCTAssertEqual(tableView.numberOfRows(inSection: 0), 1, "Expected no cell in table view")

        }

    func loadValueCell() {
        let indexPath = IndexPath(row: 0, section: 0)

        movieDetailVC.movie = movieEntries?[indexPath.row]

        let tableView = UITableView()
        tableView.dataSource = movieDetailVC
        tableView.register(MovieDetailCell.self, forCellReuseIdentifier: "MovieDetailCell")

        // giving data value

        // expected CurrencyCell class
        guard let _ = movieDetailVC.tableView(tableView, cellForRowAt: indexPath) as? MovieDetailCell else {
                   XCTAssert(false, "Expected MovieDetailCell class")
                   return
               }
    }
}
