//
//  MoviesModel.swift
//  NetworkLayer
//
//  Created by Siva Kumar Reddy Thimmareddy on 04/03/20.
//  Copyright © 2020 CocoApps. All rights reserved.
//

import Foundation

// MARK: - MoviesModel
struct MoviesModel: Codable {
    let feed: Feed?
}

// MARK: - Feed
struct Feed: Codable {
    let author: Author?
    let entry: [Entry]?
    let updated, rights, title, icon: Icon?
    let link: [FeedLink]?
    let id: Icon?
}

// MARK: - Author
struct Author: Codable {
    let name, uri: Icon?
}

// MARK: - Icon
struct Icon: Codable {
    let label: String?
}

// MARK: - Entry
struct Entry: Codable {

    let imName: Icon?
    let imImage: [IMImage]?
    let summary: Icon?
    let imPrice: IMPrice?
    let imContentType: IMContentType?
    let rights, title: Icon?
    let link: [EntryLink]?
    let id: ID?
    let imArtist: Icon?
    let category: Category?
    let imReleaseDate: IMReleaseDate?
    let imRentalPrice: IMPrice?

    enum CodingKeys: String, CodingKey {
        case imName = "im:name"
        case imImage = "im:image"
        case summary
        case imPrice = "im:price"
        case imContentType = "im:contentType"
        case rights, title, link, id
        case imArtist = "im:artist"
        case category
        case imReleaseDate = "im:releaseDate"
        case imRentalPrice = "im:rentalPrice"
    }
}

// MARK: - Category
struct Category: Codable {
    let attributes: CategoryAttributes?
}

// MARK: - CategoryAttributes
struct CategoryAttributes: Codable {
    let imID, term: String?
    let scheme: String?
    let label: String?

    enum CodingKeys: String, CodingKey {
        case imID
        case term, scheme, label
    }
}

// MARK: - ID
struct ID: Codable {
    let label: String?
    let attributes: IDAttributes?
}

// MARK: - IDAttributes
struct IDAttributes: Codable {
    let imID: String?

    enum CodingKeys: String, CodingKey {
        case imID
    }
}

// MARK: - IMContentType
struct IMContentType: Codable {
    let attributes: IMContentTypeAttributes?
}

// MARK: - IMContentTypeAttributes
struct IMContentTypeAttributes: Codable {
    let term, label: Label?
}

enum Label: String, Codable {
    case movie = "Movie"
}

// MARK: - IMImage
struct IMImage: Codable {
    let label: String?
    let attributes: IMImageAttributes?
}

// MARK: - IMImageAttributes
struct IMImageAttributes: Codable {
    let height: String?
}

// MARK: - IMPrice
struct IMPrice: Codable {
    let label: String?
    let attributes: IMPriceAttributes?
}

// MARK: - IMPriceAttributes
struct IMPriceAttributes: Codable {
    let amount: String?
    let currency: Currency?
}

enum Currency: String, Codable {
    case usd = "USD"
}

// MARK: - IMReleaseDate
struct IMReleaseDate: Codable {
    let label: String?
    let attributes: Icon?
}

// MARK: - EntryLink
struct EntryLink: Codable {
    let attributes: PurpleAttributes?
    let imDuration: Icon?

    enum CodingKeys: String, CodingKey {
        case attributes
        case imDuration
    }
}

// MARK: - PurpleAttributes
struct PurpleAttributes: Codable {
    let rel: Rel?
    let type: TypeEnum?
    let href: String?
    let title: Title?
    let imAssetType: IMAssetType?

    enum CodingKeys: String, CodingKey {
        case rel, type, href, title
        case imAssetType
    }
}

enum IMAssetType: String, Codable {
    case preview = "preview"
}

enum Rel: String, Codable {
    case alternate = "alternate"
    case enclosure = "enclosure"
}

enum Title: String, Codable {
    case preview = "Preview"
}

enum TypeEnum: String, Codable {
    case textHTML = "text/html"
    case videoXM4V = "video/x-m4v"
}

// MARK: - FeedLink
struct FeedLink: Codable {
    let attributes: FluffyAttributes?
}

// MARK: - FluffyAttributes
struct FluffyAttributes: Codable {
    let rel: String?
    let type: TypeEnum?
    let href: String?
}
