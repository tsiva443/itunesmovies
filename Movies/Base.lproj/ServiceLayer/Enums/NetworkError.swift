//
//  NetworkError.swift
//  Movies
//
//  Created by Siva Kumar Reddy Thimmareddy on 02/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

enum NetworkError {
    case unknown
    case noJSONData
}
