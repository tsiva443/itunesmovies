//
//  PostService.swift
//  Movies
//
//  Created by Siva Kumar Reddy Thimmareddy on 03/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import Foundation

enum PostService: ServiceProtocol {

    case getMoviesList

    var baseURL: URL {
        return URL(string: "https://itunes.apple.com/")!
    }

    var path: String {
        switch self {
        case .getMoviesList:
            return "us/rss/topmovies/limit=50/json"
        }
    }

    var method: HTTPMethod {
        return .get
    }

    var task: Task {
        switch self {
        case .getMoviesList:
            return .requestPlain

        }
    }

    var headers: Headers? {
        return nil
    }

    var parametersEncoding: ParametersEncoding {
        return .url
    }
}
