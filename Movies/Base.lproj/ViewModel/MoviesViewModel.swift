//
//  MoviesViewModel.swift
//  Movies
//
//  Created by Siva Kumar Reddy Thimmareddy on 04/03/20.
//  Copyright © 2020 Siva Kumar Reddy Thimmareddy. All rights reserved.
//

import Foundation

enum PostResponse<T> {
    case successWith(T)
    case failureWith(NetworkError)
}

protocol PostResponseProtocol {
    func getMoviewList<T>(type: T.Type, service: ServiceProtocol, completion: @escaping (PostResponse<T>) -> Void) where T: Decodable
    func fetchLocalMoviesList<T: Decodable>(type: T.Type, completion: @escaping (NetworkResponse<T>) -> Void)
}

class MoviesViewModel: PostResponseProtocol {
    private let sessionProvider = URLSessionProvider()

    func getMoviewList<T>(type: T.Type, service: ServiceProtocol, completion: @escaping (PostResponse<T>) -> Void) where T: Decodable {
        sessionProvider.request(type: type, service: service) { response in
            switch response {
            case let .success(posts):
                print(posts)
                completion(.successWith(posts))
            case let .failure(error):
                print(error)
                completion(.failureWith(error))
            }
        }

    }

    func fetchLocalMoviesList<T: Decodable>(type: T.Type, completion: @escaping (NetworkResponse<T>) -> Void) {
        DispatchQueue.global().async {
            sleep(3)
            let path = Bundle.main.path(forResource: "sampleMovies", ofType: "json")!
            let data = try! Data(contentsOf: URL(fileURLWithPath: path))
            guard let model = try? JSONDecoder().decode(T.self, from: data) else { return completion(.failure(.unknown)) }

            completion(.success(model))

        }
    }
}
